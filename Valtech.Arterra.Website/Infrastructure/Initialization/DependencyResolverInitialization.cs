﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using StructureMap;
using Valtech.Arterra.Marketing.DataAccess.Bootstrapper;
using Valtech.Arterra.Marketing.Implementation.Bootstrapper;

namespace Valtech.Arterra.Website.Infrastructure.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    [ModuleDependency(typeof(ServiceContainerInitialization))]
    public class DependencyResolverInitialization : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.StructureMap().Configure(ConfigureContainer);
        }

        private static void ConfigureContainer(ConfigurationExpression container)
        {

            // Marketing
            container.AddRegistry<MarketingImplementationBootstrapper>();
            container.AddRegistry<MarketingDataAccessBootstrapper>();
        }

        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public void Preload(string[] parameters)
        {
        }
    }
}