﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;

namespace Valtech.Arterra.Website.Infrastructure.Initialization
{
    [InitializableModule]
    public class AdministrativeAccountInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            if (!Roles.RoleExists("WebAdmins"))
            {
                Roles.CreateRole("WebAdmins");
                Membership.CreateUser("valtechadmin", "Welcome2021", "valtechadmin@valtech.com");
                Roles.AddUserToRole("valtechadmin", "WebAdmins");
            }

            if (!Roles.RoleExists("WebEditors"))
            {
                Roles.CreateRole("WebEditors");
                Roles.AddUserToRole("valtechadmin", "WebEditors");
            }
                
        }
        public void Uninitialize(InitializationEngine context)
        {
        }

        public void Preload(string[] parameters)
        {
        }
    }
}