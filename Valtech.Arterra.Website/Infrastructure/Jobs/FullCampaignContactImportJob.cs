using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Logging;
using EPiServer.PlugIn;
using EPiServer.Scheduler;
using EPiServer.ServiceLocation;
using Valtech.Arterra.Marketing.Manager.Recipient;

namespace Valtech.Arterra.Website.Infrastructure.Jobs
{
    [ScheduledPlugIn(
        DisplayName = "Import Contacts to Epi Campaign (Full)",
        Description = "Export contacts from WineDirect and Import into respective recipient list in Epi Campaign",
        SortIndex = 1)]
    public class FullCampaignContactImportJob : ScheduledJobBase
    {
        private readonly IRecipientManager _recipientManager;

        public FullCampaignContactImportJob()
        {
            this._recipientManager = ServiceLocator.Current.GetInstance<IRecipientManager>();
            IsStoppable = false;
        }

        public override string Execute()
        {
            OnStatusChanged("Started execution.");

            try
            {
                int processedRecordCount = this._recipientManager.Import(DateTime.Now.AddYears(-10), true);

                OnStatusChanged($"Import Completed");
                return $"Processed {processedRecordCount} contacts";
            }
            catch (Exception ex)
            {
                //Logger.Service.Log(Level.Critical, ex.Message, ex);
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}