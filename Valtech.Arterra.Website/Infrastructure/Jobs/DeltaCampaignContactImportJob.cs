using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Logging;
using EPiServer.PlugIn;
using EPiServer.Scheduler;
using EPiServer.ServiceLocation;
using Valtech.Arterra.Marketing.Manager.Recipient;

namespace Valtech.Arterra.Website.Infrastructure.Jobs
{
    [ScheduledPlugIn(
        DisplayName = "Import Contacts to Epi Campaign (15 Mins)",
        Description = "Export contacts from WineDirect and Import into respective recipient list in Epi Campaign",
        SortIndex = 1)]
    public class DeltaCampaignContactImportJob : ScheduledJobBase
    {
        private readonly IRecipientManager _recipientManager;

        public DeltaCampaignContactImportJob()
        {
            this._recipientManager = ServiceLocator.Current.GetInstance<IRecipientManager>();
            IsStoppable = false;
        }

        public override string Execute()
        {
            OnStatusChanged("Started execution.");

            try
            {
                int processedRecordCount = this._recipientManager.Import(DateTime.Now.AddMinutes(-15));

                OnStatusChanged($"Import Completed");
                return $"Processed {processedRecordCount} contacts";
            }
            catch (Exception ex)
            {
                //Logger.Service.Log(Level.Critical, ex.Message, ex);
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}