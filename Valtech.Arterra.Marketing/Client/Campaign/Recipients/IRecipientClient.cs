﻿using System;
using System.Collections.Generic;

namespace Valtech.Arterra.Marketing.Client.Campaign.Recipients
{
    public interface IRecipientClient
    {
        void AddRecipientToRecipientList(string recipientListId, string recipientId, IDictionary<string, string> recipientData, Int64 optinProcessId);

        bool IsRecipientInRecipientList(string recipientListId, string recipientId);

        void ModifyRecipientAttributesInRecipientList(string recipientListId, string recipientId, IDictionary<string, string> data);
    }
}
