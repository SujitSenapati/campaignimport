﻿namespace Valtech.Arterra.Marketing.Client.Campaign.Recipients.DTO
{
    public interface IRecipientListMembershipDto
    {
        bool IsValid { get; set; }
        string RecipientListId { get; set; }
    }
}
