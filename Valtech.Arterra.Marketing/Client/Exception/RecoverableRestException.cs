﻿namespace Valtech.Arterra.Marketing.Client.Exception
{
    /// <summary>
    /// Fatal ESL Exception
    /// </summary>
    public abstract class RecoverableRestException : RestException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecoverableRestException"/> class.
        /// </summary>
        public RecoverableRestException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecoverableRestException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public RecoverableRestException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecoverableRestException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="errorContent">Error content.</param>
        public RecoverableRestException(string message, dynamic errorContent = null) : base(message)
        {
            base.ErrorContent = errorContent;
        }
    }
}
