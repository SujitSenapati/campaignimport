﻿namespace Valtech.Arterra.Marketing.Client.Exception
{
    /// <summary>
    /// ESL Exception
    /// </summary>
    public abstract class RestException : System.Exception
    {
        /// <summary>
        /// Gets or sets the error code (HTTP status code)
        /// </summary>
        /// <value>The error code (HTTP status code).</value>
        public int? ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the error content (body json object)
        /// </summary>
        /// <value>The error content (Http response body).</value>
        public dynamic ErrorContent { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestException"/> class.
        /// </summary>
        public RestException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public RestException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="errorContent">Error content.</param>
        public RestException(string message, dynamic errorContent = null) : base(message)
        {
            ErrorContent = errorContent;
        }

    }
}
