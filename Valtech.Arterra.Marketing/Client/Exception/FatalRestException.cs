﻿
namespace Valtech.Arterra.Marketing.Client.Exception
{
    /// <summary>
    /// Fatal Rest Exception
    /// </summary>
    public class FatalRestException : RestException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FatalRestException"/> class.
        /// </summary>
        public FatalRestException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FatalRestException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public FatalRestException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FatalRestException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="errorContent">Error content.</param>
        public FatalRestException(string message, dynamic errorContent = null) : base(message)
        {
            ErrorContent = errorContent;
        }

    }
}
