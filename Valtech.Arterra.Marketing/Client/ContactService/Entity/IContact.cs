﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valtech.Arterra.Marketing.Client.ContactService.Entity
{
    public interface IContact
    {
        double? CustomerNumber { get; }

        string FirstName { get; }

        string LastName { get; }

        string ContactStatus { get; }

        DateTime? BirthDate { get; }

        string Company { get; }

        string Address { get; }

        string Address2 { get; }

        string City { get; }

        string StateCode { get; }
        
        string CountryCode { get; }

        string Phone { get; }

        string Email { get; set; }

        string EmailStatus { get; }

        DateTime? DateAdded { get; }

        DateTime? DateModified { get; }

        DateTime? LastOrderDate { get; }

        double? LifetimeValue { get; }

        double? OrderCount { get; }

        string ZipCode { get; }

        double? AvgOrder { get; }

        Int64 OptinProcessId { get;  }
    }
}
