﻿using System;
using System.Collections.Generic;
using Valtech.Arterra.Marketing.Client.ContactService.Entity;

namespace Valtech.Arterra.Marketing.Client.ContactService
{
    public interface IContactServiceClient
    {
        IDictionary<string, IList<IContact>> GetContactsByRecipientLists(DateTime dateModifiedFrom, DateTime dateModifiedTo);
    }
}
