﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valtech.Arterra.Marketing.Manager.Recipient
{
    public interface IRecipientManager
    {
        int Import(DateTime importFrom, bool isFirstTimeImport = false);
    }
}
