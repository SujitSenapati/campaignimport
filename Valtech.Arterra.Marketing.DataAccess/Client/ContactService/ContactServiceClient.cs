﻿using System;
using System.Collections.Generic;
using Valtech.Arterra.Marketing.Client.ContactService;
using Valtech.Arterra.Marketing.Client.ContactService.Entity;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration.Entity;
using Valtech.Arterra.Marketing.DataAccess.WDContactService;

namespace Valtech.Arterra.Marketing.DataAccess.Client.ContactService
{
    public class ContactServiceClient : IContactServiceClient
    {
        private readonly ContactServiceService _contactServiceClient;
        private readonly IMarketingConfigurationManager _marketingConfigurationManager;

        public ContactServiceClient(ContactServiceService contactServiceClient, IMarketingConfigurationManager marketingConfigurationManager)
        {
            this._contactServiceClient = contactServiceClient;
            this._marketingConfigurationManager = marketingConfigurationManager;
        }
        public IDictionary<string, IList<IContact>> GetContactsByRecipientLists(DateTime dateModifiedFrom, DateTime dateModifiedTo)
        {
            IDictionary<string, IList<IContact>> contactsByRecipientLists = new Dictionary<string, IList<IContact>>();

            foreach (string recipientListId in this._marketingConfigurationManager.BrandAccountsByRecipientListIds.Keys)
            {
                IList<IBrandAccount> brandAccounts =
                    this._marketingConfigurationManager.BrandAccountsByRecipientListIds[recipientListId];
                IList<IContact> contacts = new List<IContact>();
                foreach (IBrandAccount brandAccount in brandAccounts)
                {
                    double pageSize = 100;
                    double page = 0;
                    double totalCount = double.MaxValue;
                    Request11 request = new Request11()
                    {
                        Security = new Security()
                        {
                            Username = brandAccount.Username,
                            Password = brandAccount.Password
                        },
                        WebsiteIDs = brandAccount.WebsiteId,
                        DateModifiedFrom = dateModifiedFrom,
                        DateModifiedTo = dateModifiedTo,
                        MaxRows = pageSize
                    };

                    IList<string> excludeOptIns = brandAccount.ExcludeOptIns;
                    do
                    {
                        page++;
                        request.Page = page;
                        Response11 response = this._contactServiceClient.SearchContacts(request);
                        totalCount = response.RecordCount ?? 0;
                        if (response.IsSuccessful != null && response.IsSuccessful.Value && totalCount > 0)
                        {
                            foreach (Contact responseContact in response.Contacts)
                            {
                                if (!String.IsNullOrWhiteSpace(responseContact.EmailStatus) && excludeOptIns.Contains(responseContact.EmailStatus.ToLower()))
                                    continue;

                                IContact contact = new Entity.Contact()
                                {
                                    Address = responseContact.Address,
                                    Address2 = responseContact.Address2,
                                    BirthDate = responseContact.BirthDate,
                                    City = responseContact.City,
                                    Company = responseContact.Company,
                                    CountryCode = responseContact.CountryCode,
                                    CustomerNumber = responseContact.CustomerNumber,
                                    DateAdded = responseContact.DateAdded,
                                    DateModified = responseContact.DateModified,
                                    Email = responseContact.Email,
                                    EmailStatus = responseContact.EmailStatus,
                                    FirstName = responseContact.FirstName,
                                    LastName = responseContact.LastName,
                                    LastOrderDate = responseContact.LastOrderDate,
                                    LifetimeValue = responseContact.LifetimeValue,
                                    OrderCount = responseContact.OrderCount,
                                    ContactStatus = responseContact.OrderCount > 1 ? "RepeatCustomer" : (responseContact.OrderCount > 0 ? "1stTimeCustomer": "Prospect"),
                                    Phone = responseContact.Phone,
                                    StateCode = responseContact.StateCode,
                                    ZipCode = responseContact.ZipCode,
                                    AvgOrder = responseContact.OrderCount != null && responseContact.OrderCount != 0 ? responseContact.LifetimeValue / responseContact.OrderCount : 0,
                                    OptinProcessId = brandAccount.OptinProcessId
                                };

                                contacts.Add(contact);
                            }
                        }
                    } while (pageSize * page < totalCount);
                }
                
                contactsByRecipientLists.Add(recipientListId, contacts);
            }

            return contactsByRecipientLists;
        }
    }
}
