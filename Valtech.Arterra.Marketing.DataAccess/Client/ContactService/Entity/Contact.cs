﻿using System;
using System.Runtime.Serialization;
using Valtech.Arterra.Marketing.Client.ContactService.Entity;

namespace Valtech.Arterra.Marketing.DataAccess.Client.ContactService.Entity
{
    [DataContract]
    class Contact : IContact
    {
        [DataMember]
        public double? CustomerNumber { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        public string ContactStatus { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string Company { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string StateCode { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string EmailStatus { get; set; }
        
        [DataMember]
        public DateTime? DateAdded { get; set; }

        [DataMember]
        public DateTime? DateModified { get; set; }

        [DataMember]
        public DateTime? LastOrderDate { get; set; }

        [DataMember]
        public double? LifetimeValue { get; set; }

        [DataMember]
        public double? OrderCount { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        public double? AvgOrder { get; set; }

        public Int64 OptinProcessId { get; set; }
    }
}
