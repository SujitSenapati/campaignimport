﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using Valtech.Arterra.Marketing.Client.Exception;
using Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing.Exception;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration;

namespace Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing
{
    abstract class MarketingClient
    {
        private readonly IMarketingConfigurationManager _marketingConfigurationManager;
        private readonly IRestClient _restClient;

        protected MarketingClient(IMarketingConfigurationManager marketingConfigurationManager)
        {
            this._marketingConfigurationManager = marketingConfigurationManager;
            this._restClient = new RestClient(this._marketingConfigurationManager.EpiCampaignRestApiUrl);
        }

        protected string GenerateAuthToken()
        {
            string base64Decoded = string.Format("{0}:{1}", this._marketingConfigurationManager.EpiCampaignUsername, this._marketingConfigurationManager.EpiCampaignPassword);
            byte[] data = ASCIIEncoding.ASCII.GetBytes(base64Decoded);

            return Convert.ToBase64String(data);
        }

        /// <summary>
        /// Creates and sets up a RestRequest prior to a call.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="method"></param>
        /// <param name="queryParams"></param>
        /// <param name="postBody"></param>
        /// <param name="headerParams"></param>
        /// <param name="pathParams"></param>
        /// <returns></returns>
        private RestRequest PrepareRequest(
            String path, Method method, IDictionary<String, String> queryParams, String postBody, IDictionary<String, String> headerParams, IDictionary<String, String> pathParams)
        {
            RestRequest request = new RestRequest(path, method);

            // add header parameter, if any
            foreach (KeyValuePair<string, string> param in headerParams)
                request.AddHeader(param.Key, param.Value);

            // add path parameter, if any
            foreach (KeyValuePair<string, string> param in pathParams)
                request.AddParameter(param.Key, param.Value, ParameterType.UrlSegment);

            // add query parameter, if any
            if (queryParams != null)
            {
                foreach (KeyValuePair<string, string> param in queryParams)
                    request.AddQueryParameter(param.Key, param.Value);
            }

            if (postBody != null) // http body (model) parameter
                request.AddParameter("application/x-www-form-urlencoded", postBody, ParameterType.RequestBody);

            return request;
        }

        /// <summary>
        /// Makes the asynchronous HTTP request.
        /// </summary>
        /// <param name="path">URL path.</param>
        /// <param name="method">HTTP method.</param>
        /// <param name="queryParams">Query parameters.</param>
        /// <param name="postBody">HTTP body (POST request).</param>
        /// <param name="jsonObject"></param>
        /// <exception cref="RequestFailedRestException">If HTTP request failed</exception>
        /// <returns>The Task instance.</returns>
        public IRestResponse CallApi(String path, Method method, IDictionary<String, String> queryParams, String postBody)
        {
            IDictionary<string, string> headerParams = new Dictionary<String, String>();

            try
            {
                //Generate token 
                string token = GenerateAuthToken();

                headerParams.Add("Authorization", "Basic " + token);

                // Client ID must be prepended to rest api url
                path = string.Format("{0}/{1}", this._marketingConfigurationManager.EpiCampaignClientId, path);

                RestRequest request = PrepareRequest(path, method, queryParams, postBody, headerParams,
                    new Dictionary<string, string>());

                IRestResponse response = this._restClient.Execute(request);

                if (response.ErrorException != null)
                {
                    throw new RequestFailedRestException(response.ErrorException);
                }

                return response;
            }
            catch (AggregateException e)
            {
                HttpRequestException requestException = e.InnerExceptions.FirstOrDefault(c => c is HttpRequestException) as HttpRequestException;

                if (requestException != null)
                    throw new RequestFailedRestException(e);

                throw e;
            }
        }

        /// <summary>
        /// If parameter is DateTime, output in ISO8601 format.
        /// If parameter is a list of string, join the list with ",".
        /// Otherwise just return the string.
        /// </summary>
        /// <param name="obj">The parameter (header, path, query, form).</param>
        /// <returns>Formatted string.</returns>
        public string ParameterToString(object obj)
        {
            if (obj is DateTime)
                return ((DateTime)obj).ToString("u");
            else if (obj is List<string>)
                return String.Join(",", obj as List<string>);
            else
                return Convert.ToString(obj, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Deserialize the JSON string into a proper object.
        /// </summary>
        /// <param name="content">HTTP body (e.g. string, JSON).</param>
        /// <returns>Object representation of the JSON string.</returns>
        public T Deserialize<T>(string content) where T : class
        {
            // at this point, it must be a model (json)
            try
            {
                return JsonConvert.DeserializeObject<T>(content);
            }
            catch (IOException e)
            {
                throw new FatalRestException(e.Message);
            }
        }

        /// <summary>
        /// Serialize an object into JSON string.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <returns>JSON string.</returns>
        public string Serialize(object obj)
        {
            try
            {
                return obj != null ? JsonConvert.SerializeObject(obj) : null;
            }
            catch (global::System.Exception e)
            {
                throw new FatalRestException(e.Message);
            }
        }

        protected string SerializeFormEncoding(IDictionary<string, string> data)
        {
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in data)
            {
                if (builder.Length > 0)
                    builder.Append("&");

                builder.Append(pair.Key);
                builder.Append("=");
                builder.Append(pair.Value);
            }

            return builder.ToString();
        }
    }
}
