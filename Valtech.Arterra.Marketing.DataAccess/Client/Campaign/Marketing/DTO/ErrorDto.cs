﻿using System.Runtime.Serialization;

namespace Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing.DTO
{
    [DataContract]
    class ErrorDto
    {
        /// <summary>
        /// Gets or Sets Code
        /// </summary>
        [DataMember(Name = "errorCode")]
        public string Code { get; set; }
        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        [DataMember(Name = "errorMessage")]
        public string Message { get; set; }
    }
}
