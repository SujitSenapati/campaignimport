﻿namespace Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing.Exception
{
    class RequestFailedRestException : global::System.Exception
    {
        public RequestFailedRestException(global::System.Exception exception)
           : base("Request failed", exception)
        {
            
        }
    }
}
