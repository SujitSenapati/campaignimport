﻿using System;
using System.Collections.Generic;
using System.Linq;
using RestSharp;
using Valtech.Arterra.Marketing.Client.Campaign.Recipients;
using Valtech.Arterra.Marketing.Client.Exception;
using Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing;
using Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing.DTO;
using Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Marketing.Exception;
using Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Recipients.DTO;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration;

namespace Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Recipients
{
    internal class RecipientClient : MarketingClient, IRecipientClient
    {
        public RecipientClient(IMarketingConfigurationManager marketingConfigurationManager) : base(marketingConfigurationManager)
        {
        }

        public virtual void AddRecipientToRecipientList(string recipientListId, string recipientId, IDictionary<string, string> recipientData, Int64 optinProcessId)
        {
            if (string.IsNullOrEmpty(recipientListId))
                throw new ArgumentException("Invalid request parameter", "recipientList");

            if (recipientData == null)
                throw new ArgumentException("Invalid request parameter", "recipient");

            string path = "recipients/" + recipientListId;

            IDictionary<string, string> queryParams = new Dictionary<string, string>();

            recipientData.Add("optinProcessId", optinProcessId.ToString());

            string postBody = SerializeFormEncoding(recipientData);

            try
            {
                // Metrics
                IRestResponse response = new RestResponse();
                
                // make the HTTP request
                response = this.CallApi(path, Method.POST, queryParams, postBody);
                
                if (((int)response.StatusCode) == 400)
                {
                    ErrorDto error = this.Deserialize<ErrorDto>(response.Content);
                    switch (error.Code)
                    {
                        default:
                            throw new FatalRestException("Bad Request");
                    }
                }
                else if (((int)response.StatusCode) > 400)
                    throw new FatalRestException(
                        "Error calling AddRecipientToRecipientList: " + response.Content,
                        response.Content);
            }
            catch (RequestFailedRestException)
            {
                throw new FatalRestException("Request failed");
            }
        }        

        public void ModifyRecipientAttributesInRecipientList(string recipientListId, string recipientId, IDictionary<string, string> data)
        {
            if (string.IsNullOrEmpty(recipientListId))
                throw new ArgumentException("Invalid request parameter", "recipientList");

            if (string.IsNullOrEmpty(recipientId))
                throw new ArgumentException("Invalid request parameter", "recipientId");

            if (data == null)
                throw new ArgumentException("Invalid request parameter", "recipient");

            string path = string.Format("recipients/{0}/{1}", recipientListId, recipientId);

            IDictionary<string, string> queryParams = new Dictionary<string, string>();

            string postBody = SerializeFormEncoding(data);

            try
            {
                // Metrics
                IRestResponse response = new RestResponse();
                // make the HTTP request
                response = this.CallApi(path, Method.POST, queryParams, postBody);
                
                if (((int)response.StatusCode) == 400)
                {
                    ErrorDto error = this.Deserialize<ErrorDto>(response.Content);
                    switch (error.Code)
                    {
                        default:
                            throw new FatalRestException("Bad Request");
                    }
                }
                else if (((int)response.StatusCode) > 400)
                    throw new FatalRestException(
                        "Error calling ModifyRecipientAttributesInRecipientList: " + response.Content,
                        response.Content);
            }
            catch (RequestFailedRestException)
            {
                throw new FatalRestException("Request failed");
            }
        }

        public virtual bool IsRecipientInRecipientList(string recipientListId, string recipientId)
        {
            if (string.IsNullOrEmpty(recipientListId))
                throw new ArgumentException("Invalid request parameter", "recipientList");

            if (string.IsNullOrEmpty(recipientId))
                throw new ArgumentException("Invalid request parameter", "recipientId");

            string path = string.Format("recipients/{0}/{1}", recipientListId, recipientId);

            IDictionary<string, string> queryParams = new Dictionary<string, string>();
            queryParams.Add("attributeNames", "email");

            try
            {
                // Metrics
                IRestResponse response = new RestResponse();
                // make the HTTP request
                response = this.CallApi(path, Method.GET, queryParams, null);
                
                if (((int)response.StatusCode) == 400)
                {
                    ErrorDto error = this.Deserialize<ErrorDto>(response.Content);
                    switch (error.Code)
                    {
                        default:
                            throw new FatalRestException("Bad Request");
                    }
                }
                else if (((int)response.StatusCode) == 404)
                {
                    return false;   // recipient is not in the list
                }
                else if (((int)response.StatusCode) > 400)
                    throw new FatalRestException(
                        "Error calling IsRecipientInRecipientList: " + response.Content,
                        response.Content);

                return true;
            }
            catch (RequestFailedRestException)
            {
                throw new FatalRestException("Request failed");
            }
        }

        public virtual void DeleteRecipientFromRecipientList(string recipientListId, string recipientId)
        {
            if (string.IsNullOrEmpty(recipientListId))
                throw new ArgumentException("Invalid request parameter", "recipientList");

            if (string.IsNullOrEmpty(recipientId))
                throw new ArgumentException("Invalid request parameter", "recipientId");

            string path = string.Format("recipients/{0}/{1}", recipientListId, recipientId);

            IDictionary<string, string> queryParams = new Dictionary<string, string>();

            try
            {
                // Metrics
                IRestResponse response = new RestResponse();
                // make the HTTP request
                response = this.CallApi(path, Method.DELETE, queryParams, null);
                
                if (((int)response.StatusCode) == 404)
                {
                    // Recipient was not in recipient list. Ignore this erorr
                }
                else if (((int)response.StatusCode) == 400)
                {
                    ErrorDto error = this.Deserialize<ErrorDto>(response.Content);
                    switch (error.Code)
                    {
                        default:
                            throw new FatalRestException("Bad Request");
                    }
                }
                else if (((int)response.StatusCode) > 400)
                    throw new FatalRestException(
                        "Error calling DeleteRecipientFromRecipientList: " + response.Content,
                        response.Content);
            }
            catch (RequestFailedRestException)
            {
                throw new FatalRestException("Request failed");
            }
        }

        public IList<string> GetRecipientListMembership(List<string> recipientListIds, string recipientId)
        {
            string path = string.Format("recipients?recipientListIds={0}&recipientId={1}", string.Join(",", recipientListIds), recipientId);

            IDictionary<string, string> queryParams = new Dictionary<String, String>();

            try
            {
                // Metrics
                IRestResponse response = new RestResponse();
                // make the HTTP request
                response = this.CallApi(path, Method.GET, queryParams, null);
                
                if (((int)response.StatusCode) == 400)
                {
                    ErrorDto error = this.Deserialize<ErrorDto>(response.Content);
                    switch (error.Code)
                    {
                        default:
                            throw new FatalRestException("Bad Request");
                    }
                }
                else if (((int)response.StatusCode) > 400)
                    throw new FatalRestException(
                        "Error calling GetRecipientListMembership: " + response.Content,
                        response.Content);

                var membership = this.Deserialize<List<RecipientListMembershipDto>>(response.Content);
                if (membership == null || !membership.Any(m => m.IsValid))
                    return new List<string>();

                // Return all recipient lists that the recipient is a member of (valid = true)
                return membership.Where(m => m.IsValid).Select(m => m.RecipientListId).ToList();                
            }
            catch (RequestFailedRestException)
            {
                throw new FatalRestException("Request failed");
            }
        }
    }
}
