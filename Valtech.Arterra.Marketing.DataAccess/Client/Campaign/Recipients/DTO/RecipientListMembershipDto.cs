﻿using System.Runtime.Serialization;
using Valtech.Arterra.Marketing.Client.Campaign.Recipients.DTO;

namespace Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Recipients.DTO
{
    [DataContract]
    public class RecipientListMembershipDto : IRecipientListMembershipDto
    {
        /// <summary>
        /// Valid
        /// </summary>
        [DataMember(Name = "valid")]
        public bool IsValid { get; set; }

        /// <summary>
        /// RecipientListId
        /// </summary>
        [DataMember(Name = "recipientListId")]
        public string RecipientListId { get; set; }
    }
}
