﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using nJupiter.Configuration;
using StructureMap;
using StructureMap.Pipeline;
using Valtech.Arterra.Marketing.Client.Campaign.Recipients;
using Valtech.Arterra.Marketing.Client.ContactService;
using Valtech.Arterra.Marketing.DataAccess.Client.Campaign.Recipients;
using Valtech.Arterra.Marketing.DataAccess.Client.ContactService;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration;

namespace Valtech.Arterra.Marketing.DataAccess.Bootstrapper
{
    public class MarketingDataAccessBootstrapper : Registry
    {
        public MarketingDataAccessBootstrapper()
        {
            this.For<IConfigRepository>((ILifecycle) null)
                .UseIfNone<IConfigRepository>((Expression<Func<IConfigRepository>>) (() => ConfigRepository.Instance));

            //Managers
            this.For<IMarketingConfigurationManager>().Use<MarketingConfigurationManager>().Singleton();

            //Client
            this.For<IContactServiceClient>().Use<ContactServiceClient>();
            this.For<IRecipientClient>().Use<RecipientClient>();
        }
    }
}
