﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration.Entity
{
    class BrandAccount : IBrandAccount
    {
        public string Brand { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public IList<string> ExcludeOptIns { get; set; }
        public string WebsiteId { get; set; }
        public Int64 OptinProcessId { get; set; }
    }
}
