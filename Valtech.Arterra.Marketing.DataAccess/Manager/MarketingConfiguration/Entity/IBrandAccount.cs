﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration.Entity
{
    public interface IBrandAccount
    {
        string Brand { get; }
        string Username { get; }
        string Password { get; }
        IList<string> ExcludeOptIns { get; }
        string WebsiteId { get; }
        Int64 OptinProcessId { get; }
    }
}
