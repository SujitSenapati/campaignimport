﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration.Entity;

namespace Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration
{
    public interface IMarketingConfigurationManager
    {
        string EpiCampaignRestApiUrl { get; }

        string EpiCampaignClientId { get; }

        string EpiCampaignUsername { get; }

        string EpiCampaignPassword { get; }

        IDictionary<string, IList<IBrandAccount>> BrandAccountsByRecipientListIds { get; }
    }
}
