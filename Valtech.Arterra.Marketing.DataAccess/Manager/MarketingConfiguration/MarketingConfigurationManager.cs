﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using nJupiter.Configuration;
using Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration.Entity;

namespace Valtech.Arterra.Marketing.DataAccess.Manager.MarketingConfiguration
{
    public class MarketingConfigurationManager : IMarketingConfigurationManager
    {
        public string EpiCampaignRestApiUrl { get; private set; }
        public string EpiCampaignClientId { get; private set; }
        public string EpiCampaignUsername { get; private set; }
        public string EpiCampaignPassword { get; private set; }
        public IDictionary<string, IList<IBrandAccount>> BrandAccountsByRecipientListIds { get; private set; }

        public MarketingConfigurationManager(IConfigRepository configRepository)
        {
            //Initialize with the resolved configuration file
            this.InitializeByConfigurationFile(configRepository.GetConfig(this.GetType().Assembly));
        }


        public void InitializeByConfigurationFile(IConfig configuration)
        {
            this.EpiCampaignRestApiUrl = ConfigurationManager.AppSettings["Marketing:EpiCampaign:RestApiUrl"];
            this.EpiCampaignClientId = ConfigurationManager.AppSettings["Marketing:EpiCampaign:ClientId"];
            this.EpiCampaignUsername = ConfigurationManager.AppSettings["Marketing:EpiCampaign:Username"];
            this.EpiCampaignPassword = ConfigurationManager.AppSettings["Marketing:EpiCampaign:Password"];

            var apiKeysConfig = configuration.GetConfigSection("apiKeys");

            if (apiKeysConfig == null)
                return;

            IDictionary<string, IList<IBrandAccount>> brandAccountByRecipientList = new Dictionary<string, IList<IBrandAccount>>();

            try
            {
                foreach (XmlElement node in apiKeysConfig.ConfigXml.ChildNodes)
                {
                    if (node.HasAttribute("recipientlistid") && node.HasAttribute("username") && node.HasAttribute("password"))
                    {
                        Int64 optinProcessId = 0;
                        string optinProcessIdStr = node.GetAttribute("optinprocessid");

                        IBrandAccount brandAccount = new BrandAccount()
                        {
                            Brand = node.InnerText,
                            ExcludeOptIns = node.GetAttribute("excludeoptins").ToLower().Split(new []{','}, StringSplitOptions.RemoveEmptyEntries),
                            Password = node.GetAttribute("password"),
                            Username = node.GetAttribute("username"),
                            WebsiteId = node.GetAttribute("websiteid"),
                            OptinProcessId = Int64.TryParse(optinProcessIdStr, out optinProcessId) ? optinProcessId : 0
                        };

                        string recipientListId = node.GetAttribute("recipientlistid");
                        if (brandAccountByRecipientList.ContainsKey(recipientListId))
                            brandAccountByRecipientList[recipientListId].Add(brandAccount);
                        else
                            brandAccountByRecipientList.Add(recipientListId, new List<IBrandAccount>(){ brandAccount });
                    }
                }

                BrandAccountsByRecipientListIds = brandAccountByRecipientList;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}
