﻿using System;
using System.Collections.Generic;
using Valtech.Arterra.Marketing.Client.Campaign.Recipients;
using Valtech.Arterra.Marketing.Client.ContactService;
using Valtech.Arterra.Marketing.Client.ContactService.Entity;
using Valtech.Arterra.Marketing.Manager.Recipient;

namespace Valtech.Arterra.Marketing.Implementation.Manager.Recipient
{
    class RecipientManager : IRecipientManager
    {
        private readonly IContactServiceClient _contactServiceClient;
        private readonly IRecipientClient _recipientClient;

        public RecipientManager(IContactServiceClient contactServiceClient, IRecipientClient recipientClient)
        {
            this._contactServiceClient = contactServiceClient;
            this._recipientClient = recipientClient;
        }

        public int Import(DateTime importFrom, bool isFirstTimeImport)
        {
            var contactsByBrands = this._contactServiceClient.GetContactsByRecipientLists(importFrom, DateTime.Now);
            int processedRecordCount = 0;
            foreach (string recipientListId in contactsByBrands.Keys)
            {
                IList<IContact> contactsForBrand = contactsByBrands[recipientListId];

                foreach (IContact contact in contactsForBrand)
                {
                    try
                    {
                        var recipientData = this.GetMarketingRecipientData(contact);
                        if (this._recipientClient.IsRecipientInRecipientList(recipientListId, contact.Email))
                            this._recipientClient.ModifyRecipientAttributesInRecipientList(recipientListId,
                                contact.Email, recipientData);
                        else if(isFirstTimeImport || contact.DateAdded > importFrom)
                            this._recipientClient.AddRecipientToRecipientList(recipientListId, contact.Email,
                                recipientData, contact.OptinProcessId);
                        processedRecordCount++;
                    }
                    catch (Exception)
                    {

                    }
                    
                }
            }

            return processedRecordCount;
        }

        private IDictionary<string, string> GetMarketingRecipientData(IContact recipient)
        {
            var recipientData = new Dictionary<string, string>();

            recipientData.Add("recipientId", recipient.Email);
            recipientData.Add("data.email", recipient.Email);
            recipientData.Add("data.firstname", recipient.FirstName);
            recipientData.Add("data.lastname", recipient.LastName);
            recipientData.Add("data.contactstatus", recipient.ContactStatus);
            if(recipient.CustomerNumber != null)
                recipientData.Add("data.customernumber", recipient.CustomerNumber.ToString());
            recipientData.Add("data.company", recipient.Company);
            recipientData.Add("data.address", recipient.Address);
            recipientData.Add("data.address2", recipient.Address2);
            recipientData.Add("data.city", recipient.City);
            recipientData.Add("data.state", recipient.StateCode);
            recipientData.Add("data.zipcode", recipient.ZipCode);
            recipientData.Add("data.country", recipient.CountryCode);
            recipientData.Add("data.emailstatus", recipient.EmailStatus);
            recipientData.Add("data.mainphone", recipient.Phone);
            if(recipient.BirthDate != null)
                recipientData.Add("data.birthdate", recipient.BirthDate.ToString());

            double orderCount = recipient.OrderCount ?? 0;
            recipientData.Add("data.numberoftransactions", orderCount.ToString());

            double lifetimeValue = recipient.LifetimeValue ?? 0;
            recipientData.Add("data.lifetimevalue", lifetimeValue.ToString());

            double avgOrder = recipient.AvgOrder ?? 0;
            recipientData.Add("data.avgorder", avgOrder.ToString());

            if (recipient.LastOrderDate != null)
                recipientData.Add("data.lastorderdate", recipient.LastOrderDate.ToString());

            return recipientData;
        }
    }
}
