﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;
using Valtech.Arterra.Marketing.Implementation.Manager.Recipient;
using Valtech.Arterra.Marketing.Manager.Recipient;

namespace Valtech.Arterra.Marketing.Implementation.Bootstrapper
{
    public class MarketingImplementationBootstrapper : Registry
    {
        public MarketingImplementationBootstrapper()
        {
            //Managers
            this.For<IRecipientManager>().Use<RecipientManager>();

        }
    }
}
